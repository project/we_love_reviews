<?php

/*
* @file
* Adds LocalBusiness schema markup and Business Overall rating to the HTML code.
*/


/*
 *  Add Business Details form
 */

function we_love_reviews_admin_data_form($form_state) {

  $form['we_love_reviews_data'] = array(
    '#type' => 'fieldset',
    '#title' => t('Business Contact Information'),
  );

  $form['we_love_reviews_data']['we_love_reviews_business_name'] = array(
    '#title' => t('Business Name'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_name'),
    '#size' => 60,
    '#description' => t('Enter the Business Name.'),
  ); 
   $form['we_love_reviews_data']['we_love_reviews_business_address'] = array(
    '#title' => t('Address'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_address'),
    '#size' => 60,
    '#description' => t('Enter the Postal Address.'),
  );
   $form['we_love_reviews_data']['we_love_reviews_business_town'] = array(
    '#title' => t('City'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_town'),
    '#size' => 60,
    '#description' => t('Enter the City.'),
  );  
   $form['we_love_reviews_data']['we_love_reviews_business_state'] = array(
    '#title' => t('State / Province'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_state'),
    '#size' => 60,
    '#description' => t('Enter the State / Province.'),
  );  

   $form['we_love_reviews_data']['we_love_reviews_business_zip'] = array(
    '#title' => t('Postal code'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_zip'),
    '#size' => 60,
    '#description' => t('Enter the Postal Code.'),
  );
   $form['we_love_reviews_data']['we_love_reviews_business_country'] = array(
    '#title' => t('Country'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_country'),
    '#size' => 60,
    '#description' => t('Enter the Country.'),
  );
   $form['we_love_reviews_data']['we_love_reviews_business_phone'] = array(
    '#title' => t('Phone #'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_phone'),
    '#size' => 60,
    '#description' => t('Enter the Phone # in E.196 international format: +country code and no leading 0 in the number if any in your country.'),
  );
   $form['we_love_reviews_data']['we_love_reviews_business_email'] = array(
    '#title' => t('Email Address'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_email'),
    '#size' => 60,
    '#description' => t('Enter the Email Address.'),
  );

   $form['we_love_reviews_data']['we_love_reviews_business_logourl'] = array(
    '#title' => t('Logo image URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_logourl'),
    '#size' => 90,
    '#description' => t('Enter the Company Logo URL. Logo must be in square format and in high resolution (example: 690 pixels x 690 pixels).'),
  );
  $form['we_love_reviews_data']['we_love_reviews_business_url'] = array(
    '#title' => t('Website URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_url'),
    '#size' => 90,
    '#description' => t('Enter the Company Website URL.'),
  );
  $form['we_love_reviews_data']['we_love_reviews_business_sameas'] = array(
    '#title' => t('Review page URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_business_sameas'),
    '#size' => 90,
    '#description' => t('<br><a href="https://reputationcrm.com/companies-locations" title="Company / Location ID" target="_blank">Click here</a> to view the datatable with all Companies / Locations.<br>- In the datatable, locate the Company / Location and at the end of the line, click on the <strong>★&nbsp;Review Page</strong> button to open it.<br>- Copy the Review Page URL from the browser URL bar and paste it here.<br>- After pasting the URL, we recommend that you delete the language part of the URL (/en). Example:<br>&nbsp;&nbsp;&nbsp;https://welove.reviews/en/Calder-Motor-Company-EH27-8DF-East-Calder-21496426 <strong>should be saved as</strong>:<br>&nbsp;&nbsp;&nbsp;https://welove.reviews/Calder-Motor-Company-EH27-8DF-East-Calder-21496426'),
  );  
  return system_settings_form($form);
}