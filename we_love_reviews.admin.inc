<?php
/**
 * @file
 * Administrative interface file.
 */

/**
 * Implements hook_admin_settings().
 */

function we_love_reviews_admin_settings_form($form, &$form_state) {

  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('What is the We ♥ Reviews Drupal module about?'),
  );

  $form['account']['we_love_reviews_api_key'] = array(
  	'#prefix' => t('The Module NOT ONLY adds schema.org LocalBusiness contact info markup to the head section of each of your Drupal pages in JSON/LD format, BUT IT INCLUDES the up-to-date overall rating value and the total number of reviews as gathered through the ReputationCRM.com API. After configuration, you can check the schema markup on any page of your Drupal site by using the <a href="@google-testing-tool" title="Google Structured Data Testing Tool" target="_blank" />Google Structured Data Testing Tool</a>.<br><br><strong>Want more customer feedback for your business?</strong> For more info, head over to <a href="@reputation-aegis" title="Reputation Aegis" target="_blank" />Reputation Aegis</a>.',
      array(
           '@google-testing-tool' => 'https://search.google.com/structured-data/testing-tool',
           '@reputation-aegis' => 'https://reputationaegis.com/',    
        )),
    '#title' => t('API Key'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_api_key'),
    '#size' => 60,
    '#required' => TRUE,
    '#description' => t('<a target="_blank" href="@reputationcrm-apikey">Click here</a> to get your API Key.', 
         array(
         '@reputationcrm-apikey' => 'https://reputationcrm.com/settings/index/Reputation-Builder#api_server',
        )
    ),
  );  

  $form['account']['we_love_reviews_company_id'] = array(
    '#title' => t('Company / Location ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('we_love_reviews_company_id'),
    '#size' => 30,
    '#maxlength' => 30,
    '#required' => TRUE,
    '#description' => t('<a target="_blank" href="@reputationcrm-companyid">Click here</a> to view the datatable with all Companies / Locations.', 
         array(
         '@reputationcrm-companyid' => 'https://reputationcrm.com/companies-locations',
        )
    ),
  );

  $form['textfield-wrapper'] = array(
    '#type' => 'container',
    '#id' =>  'rating-check',
  );

$form['actions']['we_love_reviews_rating_check'] = array(
  '#type' => 'submit',
  '#value' => t('Check Overall Rating'),
  '#weight' => 100,
  '#ajax' => array(
      'callback' => 'we_love_reviews_rating_check_js',
      'wrapper' => 'rating-check',
      'method' => 'replace',
      'effect' => 'fade',
    ),
);

  return system_settings_form($form);
}

/**
 * Implements _form_validate().
 * We ask server if both API key & Client ID are valid via getOverallRatingAndReviewCountForPlatformReviewsByCompanyId
 * per suggestion from reputationcrm developers and we get 200 and ratings pair in response. 
 */

function we_love_reviews_admin_settings_form_validate($form, &$form_state) {

/** API: https://reputationcrm.com/developers/documentation/v2/explorer
 *   Example call: 
 *   https://reputationcrm.com/v2/getOverallRatingAndReviewCountForPlatformReviewsByCompanyId?companyid=COMPANYID&key=APIKEY
 *  
 */

 $query = array(
    'companyid' => $form_state['input']['we_love_reviews_company_id'], 
    'key' => $form_state['input']['we_love_reviews_api_key'],
   );
 $options = array('query' => $query);
 $url = url('https://reputationcrm.com/v2/getOverallRatingAndReviewCountForPlatformReviewsByCompanyId', $options);
 $options = array(
   'method'=>'GET'
  );

 $response = drupal_http_request($url, $options);
  	
 if ($response->code == '200') {
    return $response;
  }
   else {
    return form_error($form, t('Error: ' . $response->data));
  }
}


function we_love_reviews_rating_check_js($form, &$form_state) {
  
 $response = we_love_reviews_admin_settings_form_validate($form, $form_state);

 if(!empty($response->data) && $response->code == 200) {  
   
   $response_formatted = explode(',', $response->data); 
   
   $searchr = array('{"', '":' );  
   $replacer = array('<strong>', '</strong> :  ');  
   $overal = str_replace($searchr, $replacer, $response_formatted[0]);  

   $searchk = array('"R', '":', '}' );  
   $replacek = array('<strong>R', '</strong> :  ', '');  
   $rcount = str_replace($searchk, $replacek, $response_formatted[1]);
  
   $ratingcheck = '';
   $ratingcheck .= t('<h2>Values returned by the ReputationCRM.com API:</h2>');
   $ratingcheck .= t('<strong>Company / Location ID: </strong> ') . $form_state['values']['we_love_reviews_company_id'] . '<br>';
   $ratingcheck .= $overal . '<br>';
   $ratingcheck .= $rcount;
    return $ratingcheck;
  } else {
    return t('Server response error');
  }  
}

